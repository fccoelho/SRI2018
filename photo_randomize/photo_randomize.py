from random import randint


class PhotoRandomize:
    def __init__(self, photo):
        self.photo = photo

    def random(self):
        # Get the size of the image
        width, height = self.photo.size

        def add_random(n):
            new_random = n - n % 2 + randint(0, 1)
            assert abs(n - new_random) <= 1
            return new_random

        photo_new = self.photo.copy()
        # Process every pixel
        for x in range(width):
            for y in range(height):
                current_color = self.photo.getpixel((x, y))
                new_color_rgb = [add_random(pixel) for pixel in current_color[:3]]
                new_color_rgba = new_color_rgb + [current_color[-1]]
                photo_new.putpixel((x, y), tuple(new_color_rgba))
        return photo_new
