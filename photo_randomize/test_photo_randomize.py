import pytest
from PIL import Image
import matplotlib.pyplot as plt

from .photo_randomize import PhotoRandomize


def test_random_photo():
    cat_photo = Image.open('photo_randomize/little.png')
    randomizer_cat = PhotoRandomize(cat_photo)
    cat_random = randomizer_cat.random()
    assert cat_photo != cat_random

    fig = plt.figure()
    ax1 = fig.add_subplot(2, 2, 1)
    ax1.imshow(cat_photo)
    ax2 = fig.add_subplot(2, 2, 2)
    ax2.imshow(cat_random)
    plt.show()
