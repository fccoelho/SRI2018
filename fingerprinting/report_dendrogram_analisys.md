Section: image hash results

Once the two versions of the reference document were created and printed, several shots were taken using different sets of cameras, angles and lightning, most of them on very close timestamps and geolocation. The aim was to reduce the noise of different metadata (e.g. geolocation some smartphones add to their jpg files) but still get some discrepancies within both true and false sets.

The images should be classified as true or false and, within such sets, could be further segregated into rotation groups (e.g. "document centered but rotated 30 degrees), photo angle groups (e.g. "document centered and not turned, but with the camera tilted 10 degrees downward), lightning groups (e.g. "direct sunlight"), text position (e.g. "upper left quadrant") and so on.

Partially in line with such expectations, the digitized documents were grouped considering many of the previously exposed criteria. However, the main criteria was not the true-or-false aspect, but seemingly the camera used for taking the pictures - and within such criteria, sub-groups considering the several other factors affecting the images. Unfortunately, it's not possible to segregate the subgroups in true-or-false.

One way to visualize it is to observe the dendrogram.It illustrates the arrangement of the clusters based on the Hamming distance between the image�s fingerprint (p-hash) .Even though there is some soft grouping along the diagonal, one cannot see the expected two by two segregation a true-or-false criteria would be recognized for.

Looking closely along the diagonal it's possible to identify micro-groups: there are 58 groups with two to four documents 100% correlated, grouping a total of 131 documents. That is, only 20% of the documents get allocated to a 100% matching group, but such groups are so small that they do not represent any material classification of the documents.
