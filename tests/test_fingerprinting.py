__author__ = 'fccoelho'

from fingerprinting.perceptual import get_image_hashes
import imagehash
from glob import glob
from collections import Counter


def test_hashing_an_image():
    hashes = get_image_hashes('../dados/verdadeiro.jpg')
    assert isinstance(hashes, dict)
    assert len(hashes) == 4
    assert all([isinstance(h, imagehash.ImageHash) for h in hashes.values()])


def test_different_images_yield_different_hashes():
    hashes_v = get_image_hashes('../dados/verdadeiro.jpg')
    hashes_f = get_image_hashes('../dados/falso.jpg')
    for k in hashes_v:
        assert hashes_f[k] != hashes_v[k]


def test_same_images_yield_same_ahashes():
    hashes = []
    for fn in glob('../dados/Dados_Verdadeiro/*.JPG'):
        hashes.append(get_image_hashes(fn)['ahash'].__str__())
    C = Counter(hashes)
    # assert any([c > 1 for c in C.values()])
    assert len(C) == 1


def test_same_images_yield_same_phashes():
    hashes = []
    for fn in glob('../dados/Dados_Verdadeiro/*.JPG'):
        hashes.append(get_image_hashes(fn)['phash'].__str__())
    C = Counter(hashes)
    # assert any([c > 1 for c in C.values()])
    assert len(C) == 1


def test_same_images_yield_same_dhashes():
    hashes = []
    for fn in glob('../dados/Dados_Verdadeiro/*.JPG'):
        hashes.append(get_image_hashes(fn)['dhash'].__str__())
    C = Counter(hashes)
    # assert any([c > 1 for c in C.values()])
    assert len(C) == 1

def test_same_images_yield_same_whashes():
    hashes = []
    for fn in glob('../dados/Dados_Verdadeiro/*.JPG'):
        hashes.append(get_image_hashes(fn)['whash'].__str__())
    C = Counter(hashes)
    # assert any([c > 1 for c in C.values()])
    assert len(C) == 1
