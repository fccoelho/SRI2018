from collections import Counter

def count_cases(lines):
    count = Counter()
    lines_add = lines[3::5]
    for line in lines_add:
        word = line.split()[0]
        count[word] += 1
    return count

with open('verd.txt') as f:
    lines = f.readlines()
    count = count_cases(lines)
    print(count)

with open('falso.txt') as f:
    lines = f.readlines()
    count = count_cases(lines)
    print(count)